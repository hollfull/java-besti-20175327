public class CLSum1 {
     public static void main(String [] args) {
         int sum = 0;

         int [] tmp = new int [args.length];
         for(int i=0; i<args.length; i++) {
             tmp[i] = Integer.parseInt(args[i]);
         }

         for(int t : tmp){
             sum += t;

         }

         System.out.println(sum);
     }
 }
