import java.util.*;
public class MyList {
    public static void main(String [] args) {//选用合适的构造方法，用你学号前后各两名同学的学号创建四个结点
        LinkedList<String> mylist=new LinkedList<String>();
        mylist.add("20175325");
        mylist.add("20175326");
        mylist.add("20175328");
        mylist.add("20175329");
        System.out.println("遍历单链表，打印链表");//把上面四个节点连成一个没有头结点的单链表
        Iterator<String> iterator=mylist.iterator();
        while(iterator.hasNext()){
            String te=iterator.next();
            System.out.println(te);
        }//遍历单链表，打印每个结点的
        mylist.add("20175327");//把你自己插入到合适的位置（学号升序）
        Collections.sort(mylist);
        System.out.println("遍历单链表，打印插入后的链表");
        iterator=mylist.iterator();
        while(iterator.hasNext()){
            String te=iterator.next();
            System.out.println(te);
        }//遍历单链表，打印每个结点的
        mylist.remove("20175327"); //从链表中删除自己
        System.out.println("遍历单链表，打印删除后的链表");
        iterator=mylist.iterator();
        while(iterator.hasNext()){
            String te=iterator.next();
            System.out.println(te);
        }//遍历单链表，打印每个结点的
    }
}
