import java.util.Hashtable;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;
import java.util.*;

class TestMap {
    public static void main(String[] args) {

        TreeMap map1 = new TreeMap();          //TreeMap
        map1.put("李浩然", 20175318);
        System.out.println("treemap:"+map1 );
        Iterator it1 = map1.entrySet().iterator();
        Map.Entry a = (Map.Entry)it1.next();
        System.out.println("next:"+ a.getKey() +" "+a.getValue());


        HashMap map2 = new HashMap();          //HashMap
        map2.put("李浩然", 20175318);
        System.out.println("map:"+map2 );
        Iterator it2 = map2.entrySet().iterator();
        Map.Entry b = (Map.Entry)it2.next();
        System.out.println("next:"+ b.getKey() +" "+b.getValue());


        Hashtable<String, Integer> h = new Hashtable();           //Hashtable
        h.put("李浩然", 20175318);
        System.out.println("hashtable:"+h.toString());
        Iterator it3 = h.entrySet().iterator();
        Map.Entry<String, Integer> c = (Entry<String, Integer>) it3.next();
        String key = c.getKey();
        int value = c.getValue();
        System.out.println("next:"+key+" "+value);
        System.out.println(" ");




    }
}
