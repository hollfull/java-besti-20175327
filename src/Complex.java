public class Complex {
    double RealPart=0;
    double ImagePart=0;
    public Complex(){}
    public Complex(double RealPart,double ImagePart){
        this.RealPart=RealPart;
        this.ImagePart=ImagePart;

    }
    public double getRealPart(){
        return RealPart;
    }
    public double getImagePart(){
        return ImagePart;
    }
    public String toString(){
        String s = "";
        double r=RealPart;
        double i=ImagePart;
        if(r==0&&i==0){
            s="0";
        }
        else if(r==0&&i!=0){
            s=i+"i";
        }
        else if(r!=0&&i==0){
            s=r+"";
        }
        else if(r!=0&&i<0){
            s=r+""+i+"i";
        }
        else
        {
            s=r+"+"+i+"i";
        }
        return s;
    }
    public boolean equals(Object obj){
        if(this==obj){
            return true;
        }
        else return false;
    }

    public Complex ComplexAdd(Complex a){
        return new Complex(RealPart+a.getRealPart(),ImagePart+a.getImagePart());
    }
    public Complex ComplexSub(Complex a){
        return new Complex(RealPart-a.getRealPart(),ImagePart-a.getImagePart());
    }
    public Complex ComplexMulti(Complex a){
        double r=RealPart*a.getRealPart()-ImagePart*a.getImagePart();
        double i =ImagePart*a.getRealPart()+RealPart*a.getImagePart();
        return new Complex(r,i);
    }
    public Complex ComplexDiv(Complex a){
        double r=(RealPart * a.ImagePart + ImagePart * a.RealPart) / (a.ImagePart * a.ImagePart + a.RealPart * a.RealPart);
        double i=(ImagePart * a.ImagePart + RealPart * a.RealPart) / (a.RealPart * a.RealPart + a.RealPart * a.RealPart);
        return new Complex(r,i);
    }
}
