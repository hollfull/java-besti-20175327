# 20175327 2018-2019-2 《Java程序设计》第3周学习总结

## 教材学习内容
### 第四章 类与对象

> * 类变量和实例变量、import语句
> * 对象的创建过程，对象引用与实体的关系，访问权限的理解
> * this关键字：this可以出现在实例方法和构造方法中，但不可以出现在类方法中
> * 运行有包名的主类：如果主类包名为tom.jiafei.那么主类的字节码一对存放在…\tom\jiafei目录中，运行时必须打tom\jiafei的上一层目录中去运行主类
> * 类方法只能操作类变量，当对象调用类方法时，方法中的成员变量一定都是类变量，也就是说该对象和所有的对象共享类变量

## 教材学习中的问题和解决过程
> * 问题一：在建立包之后代码无法运行
> * 问题一解决方案：需要重新再建立编辑，因为字体无发识别

> * 问题二：作业中有多项重名的类名，导致无法编辑代码![](https://images.gitee.com/uploads/images/2019/0315/213435_09ab040c_4786960.jpeg "2.jpeg")
> * 问题二解决方法：一种是可以修改类名，但比较麻烦，你还需要修改代码。
一种就是建立文件夹将重名的分开到不同的文件夹里

## 代码调试中的问题和解决过程
> * 问题一：多个源文件无法编译通过![](https://images.gitee.com/uploads/images/2019/0315/213536_d896df90_4786960.png "1.png")
> * 问题一解决方法：先分别编辑保存，不要急着编译，最后javac Example4_5.java Lader.java Rect.java 一起编译才能通过然后运行就好了

> * 问题二：带包的编译找不到源文件
> * 问题二解决方法：分别通过cd 进入到文件夹里不要混乱，看准class和java分别的位置再进行编译和输出

## [托管代码](https://gitee.com/hollfull/java-besti-20175327)

运行脚本截图：
![](https://images.gitee.com/uploads/images/2019/0315/213556_96f09f20_4786960.jpeg "WechatIMG28.jpeg")
![](https://images.gitee.com/uploads/images/2019/0315/213609_1f0fb307_4786960.jpeg "WechatIMG29.jpeg")
![](https://images.gitee.com/uploads/images/2019/0315/213622_49667dd5_4786960.jpeg "WechatIMG30.jpeg")

## 上周课堂总结：
课下未完成jdb调试导致上课期间没有时间已致超时无法提交，了解到需在发出作业时及时完成

## 参考资料
> * [java学习笔记（第8版)](https://book.douban.com/subject/26371167/)
> * [《java学习笔记（第8版）》学习指导](http://www.cnblogs.com/rocedu/p/5182332.html)
