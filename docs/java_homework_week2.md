# 20175327 2018-2019-2 《Java程序设计》第2周学习总结

———

### 1.教材学习内容
第二章

> * 关于标识符的组成（注意标识符不能做标识符）
> * Java的八种基本数据类型
> * 引用变量数组的使用和注意事项（两个相同类型的数组如果具有相同的引用，他们就有完全相同的元素）

第三章

> * 各种控制语法用法（和c语言有点类似）
> * 说明了运算符和表达方式在java中表达式就是用运算符链接起来的式子
> * 语句的用法和分类（都给了说明和事例）

### 2.教材学习中的问题和解决过程

第二章
> * 问题一：通过ppt的学习还是无法完全理解输入输出数据的格式和基本操作
> * 问题一解决方案：通过上网查阅很快就得到关于输入输出格式及关于对象调用的方法
输入：![](https://images.gitee.com/uploads/images/2019/0309/190415_cd8d2ee4_4786960.jpeg "输入.jpeg")
输出：![](https://images.gitee.com/uploads/images/2019/0309/190446_19abbd8c_4786960.jpeg "输出.jpeg")
调用：![](https://images.gitee.com/uploads/images/2019/0309/190653_e7a35c90_4786960.jpeg "WechatIMG15.jpeg")
> * 问题二：数组中关于字符编码的问题有点混乱
> * 问题二解决方案：通过收集材料并通过云班课的视频得到关于符号和字符含义较全的表格
![](https://images.gitee.com/uploads/images/2019/0309/190713_da8dfc20_4786960.jpeg "表格.jpeg")

第三章
> * 问题一：逻辑运算符的注意事项
> * 问题一解决方案：通过博客文章了解到逻辑运算符是短路运算符，如op1 || op2 ，当op1是true的时候，就不再考虑op2了；op1&&op2, op1是false的时候就不再考虑op2了

### 3.代码调试中的问题和解决过程

> * 问题一：代码上传时git push出现错误
![](https://images.gitee.com/uploads/images/2019/0309/190731_2293ea13_4786960.png "git push错误.png")
> * 问题二解决方案：先git pull再git push 问题就解决了，由提示可以看出两者不同步，因此需要先pull，进行合并然后再进行push，将远程文件同步下来，然后再执行推送，就成功了
![](https://images.gitee.com/uploads/images/2019/0309/190747_31171ad5_4786960.png "git pull.png")

[托管代码](https://gitee.com/hollfull/java-besti-20175327)
运行脚本截图如下图：
![](https://images.gitee.com/uploads/images/2019/0309/190807_535f2265_4786960.jpeg "WechatIMG6.jpeg")

### 3.上周课堂总结：
> * 课堂作业超时（主要原因是没有提前准备代码，上课时打代码出错较多）

### 4.参考资料
> * [java学习笔记（第8版)](https://book.douban.com/subject/26371167/)
> * [《java学习笔记（第8版）》学习指导](http://www.cnblogs.com/rocedu/p/5182332.html)