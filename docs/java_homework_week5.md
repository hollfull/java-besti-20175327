# 20175327 2018-2019-2 《Java程序设计》第5周学习总结

------
## 1.教材学习内容总结
* 接口的接口体中只可以有常量和abstract方法
* 和类一样，接口也是java中一种重要的引用型数据类型
* 在使用多态设计程序时，要熟练使用接口回调技术以及面向接口编程的思想，以便体现程序设计所提倡的“开-闭”原则
* Collection和Map架构
* Collection
* java.util.List接口中，记录每个对象的索引顺序，依照索引取回对象。
* java.util.Set接口中，收集对象不重复，并具有集合的行为。
* java.util.Queue接口中，收集对象时以队列方式，收集的对象加入尾端，取得对象时从前端。
* java.util.Deque接口中，对Queue 的两端进行加入、移除等操作。
* Map即是以键值对形式的存放数据的容器，主要以哈希Map作为其常用的实现类。Lambada和泛型lambada
* 故若想针对某类定义的行为操作，必须告诉编译程序，让对象重新扮演该类型。JDK5之后增加了泛型语法。若接口支持泛型，在操作时也会比较方便，只要声明参考时有指定类型，那么创建对象时就不用再写类型了。

## 2.教材学习中的问题和解决过程
* 问题一：使用JDB进行调试时查看源代码的命令是（list）。
* 问题一解决方法：查阅资料，上网自行解决。
* 问题二：用enum定义一个Season的类型表示四季。
* 问题二解决方法：（public enum Season{SPRING, SUMMER, AUTUMN,WINTER}）。

## 3.代码调试中的问题和解决过程
* 问题一：语句抛出的异常，用户程序自定义的异常和应用程序特定的异常,必须借助于 throws 和 throw 语句来定义抛出异常。
* 问题一解决方法：throw是语句抛出一个异常。语法：throw (异常对象);throws是方法可能抛出异常的声明。(用在声明方法时，表示该方法可能要抛出异常)

## 4.[托管代码](https://gitee.com/hollfull/java-besti-20175327)![](https://images.gitee.com/uploads/images/2019/0328/132753_762517d2_4786960.jpeg "WechatIMG2.jpeg")![](https://images.gitee.com/uploads/images/2019/0328/132815_f92252e7_4786960.jpeg "WechatIMG3.jpeg")

## 5.上周考试错题总结
 * 父类的protected方法，在子类中可以override为public的方法。（OK)

## 6.上周课堂总结：
未能及时完成练习，让我认识到学习Java应在时间上下功夫，认真理解每个符号，每个语法，才能有量变到质变的收获。

## 参考资料
 * [java学习笔记（第8版)](https://book.douban.com/subject/26371167/)
 * [《java学习笔记（第8版）》学习指导](http://www.cnblogs.com/rocedu/p/5182332.html)
