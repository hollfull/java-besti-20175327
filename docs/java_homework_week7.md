# 20175327 2018-2019-2 《Java程序设计》第7周学习总结

------
## 1.教材学习内容总结
###第八章 常用实用类
* string类
   * 并置
    * 两个常量进行并置，得到的仍是常量。
           public class Example8_1 {
                public static void main(String args[]) {
                  String hello = "你好";
                  String testOne = "你"+"好";             
                  System.out.println(hello == testOne);   
                  System.out.println("你好" == testOne);  
                  System.out.println("你好" == hello); 
                }
           }   

* equals
    * String对象调用equals（String s）方法比较当前String对象的字符序列是否与参数s指定的String对象的字符序列相同。
* StringBuffer类
* Date类与Calendar类
* 日期格式化
    *  format方法（也可用于数字格式化中）


## 2.教材学习中的问题和解决过程
* 问题一：书上P179中的Example8_2.java中System.out.println(s1==s2);输出为false![](https://gitee.com/uploads/images/2019/0411/084852_d359a0a9_4786960.png "1.png")
* 问题一解决方案：通过查看书内容，得知string对象s1s2中存放的是引用，表明自己的实体的位置信息，因此应该使用System.out.pritnln(s1.equals(s2));
* 问题二：在学习equals时联想到=,同时回想起以前课上老师提起过instanceof，不禁产生疑问，这三个有什么区别，使用的环境有什么不同？
* 问题二解决方案：
    * instanceof：
用于判断一个引用类型所引用的对象是否是一个类（子类）的实例，左边操作元是一个引用类型，右边是一个类（父类）名或（父类实现的）接口名；
多态性，对于引用类型变量，java编译器只根据变量被显示声明的类去编译，左边操作元被显示声明的类型与右边操作元必须是同类或有继承关系
在运行时根据左边实际引用的对象来判断
    * 操作符==
操作符==用于判断两个操作元是否相等，既可以是基本类型也可是引用类型，当是后者时，引用变量必须引用同一个对象时才返回true
操作符==同样具有多态性
    * 对象的equals()方法
Object中equals方法规则为：当参数object引用的对象与当前对象为同一个对象时就返回true
jdk中一些覆盖equals方法的类有io.File、util.Date、lang.String以及包装类，比较规则更具实际意义：对象已知且内容一致，则返回true
用户可通过自定义类中覆盖equals方法，重新定义比较规则


## 3.代码调试中的问题和解决过程
* 问题一：书上例子8—23中RedEnvelope.java:1: 错误: 需要class, interface或enum![](https://gitee.com/uploads/images/2019/0411/084905_e072f393_4786960.jpeg "2.jpeg")
* 问题一解决方案：在代码中去处public即可
## 4.[托管代码](https://gitee.com/hollfull/java-besti-20175327)![](https://gitee.com/uploads/images/2019/0411/084924_705d65a8_4786960.jpeg "WechatIMG4.jpeg")
![](https://gitee.com/uploads/images/2019/0411/084947_9906004f_4786960.jpeg "WechatIMG5.jpeg")
## 5.上周考试错题总结
无，但是下周开始会在课堂测试
 
## 6.上周课堂总结：
未能及时完成练习，让我认识到学习Java应在时间上下功夫，认真理解每个符号，每个语法，才能有量变到质变的收获。

## 参考资料
 * [java学习笔记（第8版)](https://book.douban.com/subject/26371167/)
 * [《java学习笔记（第8版）》学习指导](http://www.cnblogs.com/rocedu/p/5182332.html)

