# 20175327 2018-2019-2 《Java程序设计》第10周学习总结

------
## 1.教材学习内容总结
###第十二章 Java多线程机制
* 创建线程的方式有三种，分别是：
   - 继承Thread类创建线程，程序中如果想要获取当前线程对象可以使用方法：Thread.currentThread();如果想要返回线程的名称，则可以使用方法：getName();
   - 实现Runnable接口创建线程
   - 使用Callable和Future创建线程
* 线程常用方法
start()
   * run()定义线程线程对象被调度之后所执行的操作
   * sleep(int millsecond),必须在try-catch语句块中调用sleep方法
   * isAlive()
```
Thread thread = new Thread(target);
threrad.start();
```
* 线程同步
* 线程联合
```
B.join();
```
* 计时器线程，Timer类，在javax.swing中

## 2.教材学习中的问题和解决过程

* 问题一：看到书上P382提到了计时器Timer类，该类在javax.swing包中，java.util包中也有一个名字是Timer类，产生疑惑，那java.util和javax.swing里的Timer有什么不同吗？
* 问题一解决方案：先在书上浏览了一下，没有相关信息，然后通过百度查找相关资料，得出以下结论：
   * 在 1.3 版本中，向 Java 平台添加了另一个 Timer 类：java.util.Timer。该类和 javax.swing.Timer 的基本功能相同，但是 java.util.Timer 更常用，功能更多。javax.swing.Timer 有两个特征，它们可以让使用 GUI 更方便。首先，其事件处理程序都是 GUI 程序员所熟悉的，并且可以更简单地处理事件指派线程。第二，其自动线程共享意味着不必采取特殊步骤来避免生成过多线程。相反，计时器使用同一个线程让光标闪烁、使工具提示显示等等。 

* 问题二：主线程与子线程有什么区别？
* 问题二解决方案：
```
每个线程都有一个唯一标示符，来区分线程中的主次关系的说法。 线程唯一标示符：Thread.CurrentThread.ManagedThreadID;
UI界面和Main函数均为主线程。
被Thread包含的“方法体”或者“委托”均为子线程。
委托可以包含多个方法体，利用this.Invoke去执行。
也可以定义多种方法体，放在Thread里面去执行。则此方法体均为子线程。注意如果要修改UI界面的显示。则需要使用this.Invoke，否则会报异常。
Main函数为主线程，id标示符与UI界面主线程相等。
```

## 3.代码调试中的问题和解决过程
* 问题一：书上例子12-14中出现类是公共的，应在文件中说明
![](https://gitee.com/uploads/images/2019/0502/213740_9735ab49_4786960.jpeg "WechatIMG14.jpeg")
* 问题一解决方案：在代码中去处public即可

## 4.[托管代码](https://gitee.com/hollfull/java-besti-20175327)
![](https://gitee.com/uploads/images/2019/0502/213755_095aef3b_4786960.jpeg "WechatIMG15.jpeg")
![](https://gitee.com/uploads/images/2019/0502/213810_fa828048_4786960.jpeg "WechatIMG16.jpeg")

## 5.上周考试错题总结
```
下列属于常用数据库的是 
A Access
B XAMMP
C .MySQL
D .Oracle
E .SQL Server
```
正确答案： A C D E 你的答案： C D E
```
下列属于数据操作语言的是 
A insert
B update
C create
D select
```
正确答案： A B 你的答案： A B D

## 6.上周课堂总结：
未能及时完成练习，让我认识到学习Java应在时间上下功夫，认真理解每个符号，每个语法，才能有量变到质变的收获。

## 参考资料
 * [java学习笔记（第8版)](https://book.douban.com/subject/26371167/)
 * [《java学习笔记（第8版）》学习指导](http://www.cnblogs.com/rocedu/p/5182332.html)


