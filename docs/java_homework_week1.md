# 20175327 2018-2019-2 《Java程序设计》第一周学习总结
# 教材学习内容总结
> * 学习第一章视频
> * 在Mac上使用终端运行教材第一章代码Hello.java和People.java
> * 下载使用git，按照使用码云和博客园学习简易教程和码云使用手册完成码云的托管代码
> * 在git上完成教材第一章代码的运行及编译，添加运行并截图上传
# 教材学习中的问题和解决过程
> * 问题1：git下载完成的时候，我们对其进行双击安装，但是会出现这样的提示：![](https://images.gitee.com/uploads/images/2019/0303/211127_06dcedfa_4786960.jpeg "WechatIMG6.jpeg")
> * 问题一解决方案：按住control，然后点击我们要安装的git包，点击打开就可以安装了。
> * 问题二：git下载完成后，安装完成后，就可以进行git clone项目编码，但是每次git clone都会显示在finder里面的我的所有文件中，必须在一个指定的文件下打开终端命令窗口。
> * 问题二解决方案：在偏好设置中打开键盘-快捷键-服务，新建文件夹的终端窗口和终端标签页前打勾，如图：![](https://images.gitee.com/uploads/images/2019/0303/211244_f109e13f_4786960.jpeg "WechatIMG7.jpeg")
然后选中文件夹，在服务中即可打开终端窗口。
# 代码调试中的问题和解决过程
> * 问题一：不理解Hello_World里面代码的具体含义。
> * 问题一解决方案：还是利用百度浏览器查找相关内容并学习分析。
# [代码托管](https://gitee.com/hollfull/java-besti-20175327/tree/master)
![](https://images.gitee.com/uploads/images/2019/0303/211258_62b96601_4786960.jpeg "WechatIMG8.jpeg")
![](https://images.gitee.com/uploads/images/2019/0303/211313_64a2f953_4786960.jpeg "WechatIMG9.jpeg")
![](https://images.gitee.com/uploads/images/2019/0303/211326_bc8a621b_4786960.jpeg "WechatIMG10.jpeg")