# 20175327 2018-2019-2 《Java程序设计》第8周学习总结

------
## 1.教材学习内容总结
### 第十五章 泛型与集合框架
* 泛型
*     声明泛型类class 名<泛型列表>
*     声明对象：类名多了<>，并用具体类型替换<>中的泛型
* LinkedList泛型类（创建链表）
* HashMap<K,V>泛型类（其对象采用散列表这种数据结构存储数据）
* 树集
*     TreeSet泛型类（其创建的对象称作树集）
*     树映射（适合用于数据的排序，结点按着存储对象的大小升序排列）

## 2.教材学习中的问题和解决过程
* 问题一：在学习equals时联想到=,同时回想起以前课上老师提起过instanceof，不禁产生疑问，这三个有什么区别，使用的环境有什么不同？
* 问题一解决方案：通过查资料得：
```
instanceof：

用于判断一个引用类型所引用的对象是否是一个类（子类）的实例，左边操作元是一个引用类型，右边是一个类（父类）名或（父类实现的）接口名；
多态性，对于引用类型变量，java编译器只根据变量被显示声明的类去编译，左边操作元被显示声明的类型与右边操作元必须是同类或有继承关系
在运行时根据左边实际引用的对象来判断


操作符==

操作符==用于判断两个操作元是否相等，既可以是基本类型也可是引用类型，当是后者时，引用变量必须引用同一个对象时才返回true
操作符==同样具有多态性



对象的equals()方法

Object中equals方法规则为：当参数object引用的对象与当前对象为同一个对象时就返回true
jdk中一些覆盖equals方法的类有io.File、util.Date、lang.String以及包装类，比较规则更具实际意义：对象已知且内容一致，则返回true
用户可通过自定义类中覆盖equals方法，重新定义比较规则
```

## 3.代码调试中的问题和解决过程
* 问题一：成功编译，但是未能成功运行
* 问题一解决方案：尝试了在代码开头添加import java.util.*在运行时应该直接java C

## 4.[托管代码](https://gitee.com/hollfull/java-besti-20175327)
![](https://images.gitee.com/uploads/images/2019/0420/215344_f7bffa40_4786960.jpeg "WechatIMG2.jpeg")
![](https://images.gitee.com/uploads/images/2019/0420/215354_588d6975_4786960.jpeg "WechatIMG3.jpeg")

## 5.上周考试错题总结
* 下面哪些Linux 命令可以ASCII码和16进制单字节方法输出Hello.java的内容？

     A .od -b -tx1 Hello.java

     B .od -tcx1 Hello.java

     C .od -tc -tx1 Hello.java

     D .od -tbx1 Hello.java

正确答案： B C
 
* 下面哪些内容会出现在Period对象中？

   A .Year

   B .Month

   C .Day

   D .Hour

   E .Minute

   F .Second
正确答案： A B C
## 6.上周课堂总结：
未能及时完成练习，让我认识到学习Java应在时间上下功夫，认真理解每个符号，每个语法，才能有量变到质变的收获。

## 7.参考资料
 * [java学习笔记（第8版)](https://book.douban.com/subject/26371167/)
 * [《java学习笔记（第8版）》学习指导](http://www.cnblogs.com/rocedu/p/5182332.html)

