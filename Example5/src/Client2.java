import java.io.*;
import java.net.*;

public class Client2 {
    public static void main(String args[]) {
        System.out.println("客户端启动...");
        //while (true) {
        Socket mysocket;
        DataInputStream in = null;
        DataOutputStream out = null;
        try {
            mysocket = new Socket("192.168.43.35", 2010);
            in = new DataInputStream(mysocket.getInputStream());
            out = new DataOutputStream(mysocket.getOutputStream());
            System.out.println("请输入:");
            String str = new BufferedReader(new InputStreamReader(System.in)).readLine();
            MyBC turner = new MyBC();
            turner.conversion(str);
            String str1 = turner.getMessage();
            out.writeUTF(str1);
            String s = in.readUTF();   //in读取信息，堵塞状态
            System.out.println("客户收到服务器的回答:" + s);
            Thread.sleep(500);
        } catch (Exception e) {
            System.out.println("服务器已断开" + e);
        }
        //}
    }
}
