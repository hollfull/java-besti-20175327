import java.util.*;

public class MyBC {
    private Stack<String> stack;
    private List<String> list;

    private String message, Message = "";

    public MyBC() {
        stack = new Stack<String>();//用来暂时存放运算符的栈
        list = new ArrayList<String>();//用来暂时存放操作数及运算符的列表
    }

    public void conversion(String expr) {   //中缀转后缀
        String token;
        StringTokenizer tokenizer = new StringTokenizer(expr);

        while (tokenizer.hasMoreTokens()) {
            //当tokenizer有下一个值时，进行循环，并把值赋给token
            token = tokenizer.nextToken();

            if (token.equals("(")) {
                //如果是左括号，入栈
                stack.push(token);
            } else if (token.equals("+") || token.equals("-")) {
                //如果是“+”或“-”，继续判断栈是否为空
                if (!stack.empty()) {
                    //如果栈非空，判断栈顶元素是什么
                    if (stack.peek().equals("(")) {
                        //如果栈顶为“（”，运算符入栈
                        stack.push(token);
                    } else {
                        //否则先把栈顶元素移除，加到列表中，再将运算符入栈
                        list.add(stack.pop());
                        stack.push(token);
                    }
                } else {
                    //若栈为空，运算符入栈
                    stack.push(token);
                }
            } else if (token.equals("*") || token.equals("÷")) {
                //如果是“*”或“÷”，继续判断栈是否为空
                if (!stack.empty()) {
                    //如果栈非空，判断栈顶元素是什么
                    if (stack.peek().equals("*") || stack.peek().equals("÷")) {
                        //如果栈顶为“*”或“÷”，先把栈顶元素移除，加到列表中，再将运算符入栈
                        list.add(stack.pop());
                        stack.push(token);
                    } else {
                        //如果栈顶为其他，运算符直接入栈
                        stack.push(token);
                    }
                } else {
                    //如果栈为空，运算符直接入栈
                    stack.push(token);
                }
            } else if (token.equals(")")) {
                //如果遇到“）”，开始循环
                while (true) {
                    //先把栈顶元素移除并赋给A
                    String A = stack.pop();
                    if (!A.equals("(")) {
                        //如果A不为“（”，则加到列表
                        list.add(A);
                    } else {
                        //如果A为“（”，退出循环
                        break;
                    }
                }
            } else {
                //如果为操作数，进入列表
                list.add(token);
            }
        }
        while (!stack.empty()) {
            //将栈中元素取出，加到列表中，直到栈为空
            list.add(stack.pop());
        }
        ListIterator<String> li = list.listIterator();//返回此列表元素的列表迭代器（按适当顺序）。
        while (li.hasNext()) {
            //将迭代器中的元素依次取出，并加上空格作为分隔符
            Message += li.next() + " ";
            li.remove();
        }
        message = Message;
    }

    public String getMessage() {
        return message;
    }
}